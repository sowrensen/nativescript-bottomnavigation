import * as app from "application";
import { GridLayout } from "ui/layouts/grid-layout";
import { Ripple } from "nativescript-ripple";
import { AppRootViewModel } from "~/app-root-view-model";
import { topmost } from "ui/frame";

export class Resources {

	/**
	 * Hide the navigation bar during a view-to-view navigation
	 */
	public hideNavBar() {
		// if the navigation stack is not empty the navigation bar is hidden already
		if (topmost().backStack.length >= 1) {
			return;
		}
		let rootView = <GridLayout>app.getRootView();
		let navBar = <GridLayout>rootView.getChildAt(1);
		navBar.set("visibility", "collapsed");
	}

	/**
	 * Show the navigation bar on back key press from view-to-view navigation
	 */
	public showNavBar() {
		// do not unhide the navigation bar until only one activity left in the stack
		if (topmost().backStack.length > 1) {
			return;
		}
		let rootView = <GridLayout>app.getRootView();
		let navBar = <GridLayout>rootView.getChildAt(1);
		navBar.set("visibility", "visible");
	}

	/**
	 * This method simulates a navigation to the specified menu using the core
	 * navigation method. Use this method for view-to-view navigation.
	 * @param string The menu to be navigated, expected an ID
	 */
	public simulateNavigation(menu: string) {
		let rootView = <GridLayout>app.getRootView();
		let navBar = <GridLayout>rootView.getChildAt(1);
		let tappedView = <any>navBar.getViewById(menu);
		new AppRootViewModel().navigate({
			eventName: "tap",
			object: tappedView
		});
	}
}