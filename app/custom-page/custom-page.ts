import { Page } from "ui/page";
import { EventData, fromObject } from "data/observable";
import { topmost } from "tns-core-modules/ui/frame/frame";
import { Resources } from "~/modules/resources";


export function onNavigatedTo(args: EventData) {
	let page = <Page>args.object;
	page.bindingContext = fromObject({

	});
}

export function goDeeper() {
	topmost().navigate({
		moduleName: 'second-page/second-page',
		backstackVisible: true // backStackVisible must be enabled
	});
	// this method will not affect any longer, since navbar is hidden already
	// user does not need to call it furthermore
	new Resources().hideNavBar();
}

export function getBackStackEntry() {
	console.log(topmost().backStack.length);
}