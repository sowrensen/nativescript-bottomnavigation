import { Page } from "ui/page";
import { EventData, fromObject } from "data/observable";
import { BackstackEntry, topmost } from "ui/frame";

export function onNavigatedTo(args: EventData) {
	let page = <Page>args.object;
	page.bindingContext = fromObject({

	});
}

export function getBackStackEntry() {
	console.log(topmost().backStack.length);
}