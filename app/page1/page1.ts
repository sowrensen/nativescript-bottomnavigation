import { Page } from "ui/page";
import { EventData, fromObject } from "data/observable";
import { topmost } from "tns-core-modules/ui/frame/frame";
import { Resources } from "~/modules/resources";

export function onNavigatedTo(args: EventData) {
	let page = <Page>args.object;
	page.bindingContext = fromObject({
	});
}

export function goToPage2() {
	// simulate a navigation to page2
	new Resources().simulateNavigation("page2");
}