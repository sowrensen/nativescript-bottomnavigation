import { Page } from "ui/page";
import { EventData, fromObject } from "data/observable";
import { Resources } from "~/modules/resources";
import { topmost } from "tns-core-modules/ui/frame/frame";

export function onNavigatedTo(args: EventData) {
	let page = <Page>args.object;
	page.bindingContext = fromObject({});
}

export function goToCustomPage() {
	// performing a secondary navigation
	topmost().navigate({
		moduleName: "custom-page/custom-page",
	});
	// once user called this method once, user doesn't need to call it further in the same stack
	new Resources().hideNavBar();
}