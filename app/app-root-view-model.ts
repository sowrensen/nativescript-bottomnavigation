import { Observable, EventData } from "data/observable";
import { Ripple } from "nativescript-ripple";
import { GridLayout } from "ui/layouts/grid-layout";
import { topmost } from "tns-core-modules/ui/frame/frame";
import * as app from "application";

export class AppRootViewModel extends Observable {

	// NOTE: setting previousMenu and tappedMenu to any is recommended, 
	// so that the methods can work with any component defined in xml
	private static previousMenu: any;

	constructor() {
		super();
	}

	/**
	 * Controls the primary navigation of the application
	 * @param args Tapped menu context
	 */
	public navigate(args: EventData) {
		let tappedMenu = <any>args.object;
		let rootView = <GridLayout>app.getRootView();
		let bottomNavigationContainer = <GridLayout>rootView.getChildAt(1);

		// if some login restriction to be put, implement here and modify tappedRippleView

		if (tappedMenu == AppRootViewModel.previousMenu) {
			return;
		} else {
			AppRootViewModel.previousMenu = tappedMenu;
		}

		bottomNavigationContainer.eachChildView((view) => {
			view.set("class", "bottom-nav-btn");
			return true;
		});

		topmost().navigate({
			moduleName: `${tappedMenu.id}/${tappedMenu.id}`,
			clearHistory: true,
		});
		tappedMenu.set("class", "bottom-nav-btn bottom-nav-btn-active");
	}
}