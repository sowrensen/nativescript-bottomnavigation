import { Page } from "ui/page";
import { EventData, fromObject } from "data/observable";

export function onNavigatedTo(args: EventData) {

	let page = <Page>args.object;
	page.bindingContext = fromObject({

	});

}