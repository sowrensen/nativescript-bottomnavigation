#NativeScript Bottom Navigation
This application is an example of implementing custom **Bottom Navigation Bar** in NativeScript.


##Implementation

###RootView (XML)
A `GridLayout` is used as the root view. It contains two children: a `Frame` and a `GridLayout`. The frame holds the default page of the application. And the GridLayout is defined as the bottom nav bar. Every child of the NavBar (GridLayout) will specify a tap event. Use some font icon to define icon for navigation buttons. In this example, Material Icon font is used. Here I used nativescript-ripple as the container for each menu. Feel free to use any component(e.g. StackLayout, GridLayout, etc.) you want. You don't need to change anything at all in the code behind. But make sure that you are only using one type of component for all of the menus. Or it will not work.

**N.B.:** You should not change rest of the layout. A GridLayout should be the parent, which holds the `Frame` and another `GridLayout` as children.

###RootViewModel (TS)
The specified tap event in XML will be defined in this file. It will retrieve the tapped element, and will return if the same element is tapped continuously. Otherwise,  all children of NavBar will be marked as `deselected` prior to mark the tapped item as `selected`. You can change color (as shown in the example) to mark select or deselect. 

###View-To-View Navigation
View-to-View navigation is revamped on version 2. The navigation system is divided into two section:

* Primary Navigation - That controls the main navigation of the app.

* Secondary Navigation - That used to perform some secondary tasks and should move back to main navigation.

####Primary Navigation
Use `simulateNavigation` for primary view-to-view navigation. It simulates an actual navigation and uses the core method to navigate, hence creates no consistency error. Just call the method and pass the desired menu to be navigated to in the argument. Voila! :D

####Secondary Navigation
Whenever you need to navigate to some view which is not a part of main navigation, use this one. For example from your contact view, you can open a message view which is not specified in main navigation. Just navigate using `topmost().navigate()` method and then call `hideNavBar()` to hide the navigation bar. On the back key press the navigation bar should unhide and your application should return to main navigation system. If you need to navigate further in the same stack, you do not need to call `hideNavBar()` any more. Please note that, `backstackVisible` navigational property must be set to `true`, either it will create inconsistency. So when navigating further, either explicitly set `backstackVisible: true` or don't set this property even, it is true as default.

**N.B.:** Helper functions should be defined in a separate file/module. 
